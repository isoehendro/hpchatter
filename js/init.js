/*global window, $j, document, escape, errorCallback, sessionCallback, url,  $, unescape, forcetk, clientId, loginUrl, proxyUrl, console, Force, addClickListeners, getRecords, redirectUri */
console.log("=> init.js");

var client = new forcetk.Client(clientId, loginUrl, proxyUrl);
//Force.init(null, null, client, null);	

// We use $j rather than $ for jQuery
if (window.$j === undefined) { $j = $; }

function getAuthorizeUrl(loginUrl, clientId, redirectUri) {
	'use strict';
    return loginUrl + 'services/oauth2/authorize?display=touch'
        + '&response_type=token&client_id=' + escape(clientId)
        + '&redirect_uri=' + escape(redirectUri);
}

function sessionCallback(oauthResponse) {
	'use strict';
    console.log("=> sessionCallback");
    if (typeof oauthResponse === 'undefined' || typeof oauthResponse.access_token === 'undefined') {
        errorCallback({
            status: 0,
            statusText: 'Unauthorized',
            responseText: 'No OAuth response'
        });
    } else {
		succesfullAuthentication(oauthResponse);
    }
}

function succesfullAuthentication(oauthResponse) {
	console.log("=> succesfullAuthentication");
	client.setSessionToken(oauthResponse.access_token, null, oauthResponse.instance_url);
	console.log("=> SUCCESS Authentication")
}

$j(document).ready(function () {
    console.log('=> Document ready ' + window.location.href);
    if (client.sessionId === null) {
        var message, oauthResponse, nvps, i, parts;
		oauthResponse = {};
		//if token exists in the URL, parse it, keep it in oauthResponse, and get access token via sessionCallback function
        if (window.location.hash && window.location.href.indexOf('access_token') > 0) {
            message = window.location.hash.substr(1);
            nvps = message.split('&');
            for (i = 0; i < nvps.length; i = i + 1) {
                parts = nvps[i].split("=");
                oauthResponse[parts[0]] = unescape(parts[1]);
            }
            if (oauthResponse.access_token) { sessionCallback(oauthResponse); }
        } else {
            url = getAuthorizeUrl(loginUrl, clientId, redirectUri);
            window.location.href = url;
        }
    }
});


